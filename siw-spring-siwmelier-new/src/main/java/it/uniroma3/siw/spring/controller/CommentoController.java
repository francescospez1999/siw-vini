package it.uniroma3.siw.spring.controller;

import java.security.Principal;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.spring.model.Commento;
import it.uniroma3.siw.spring.model.Credentials;
import it.uniroma3.siw.spring.model.Vino;
import it.uniroma3.siw.spring.service.CommentoService;
import it.uniroma3.siw.spring.service.CredentialsService;
import it.uniroma3.siw.spring.service.VinoService;

@Controller
public class CommentoController {

	@Autowired
	private CommentoService commentoService;

	@Autowired
	private CredentialsService credentialsService;

	@Autowired
	private VinoService vinoService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private Credentials getCredentialsCorrente() {
		User current = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return this.credentialsService.getCredentials(current.getUsername());
	}

	/**AGGIUNTA**/
	/*Popola la form*/
	@RequestMapping(value="/addCommento/{id}", method = RequestMethod.GET)
	public String addCommento(Model model, @PathVariable("id")Long id) {
		logger.debug("PASSO ALLA FORM addCommento");
		model.addAttribute("commento", new Commento());
		model.addAttribute("vino", this.vinoService.vinoPerId(id));
		return "commentoForm.html";
	}

	/*raccoglie e valida i dati della form*/
	@RequestMapping(value = "/inserisciCommento/{id}", method = RequestMethod.POST)
	public String newCommento(@ModelAttribute("commento") Commento commento, 
			@PathVariable("id")Long id,
			Model model, BindingResult bindingResult) {
		Credentials utenteCorrente = this.getCredentialsCorrente();
		Vino vinoCorrente = this.vinoService.vinoPerId(id);
		commento.setUtente(utenteCorrente);
		commento.setVino(vinoCorrente);
		commento.setData(LocalDate.now());
		
		this.commentoService.inserisci(commento);

		return "redirect:/vino/"+id;
	} 


	/**MODIFICA**/
	@RequestMapping(value="/editCommento/{id}", method = RequestMethod.GET)
	public String editCommento(@PathVariable("id") Long id, Model model) { 
		Credentials utenteCorrente = this.getCredentialsCorrente();
		Commento commento = this.commentoService.commentoById(id);
		
		
		if(this.commentoService.tuttiPerCredentials(utenteCorrente).contains(commento) || this.getCredentialsCorrente().getRole().equals("ADMIN")) {
			model.addAttribute("commento",commento);
			return "/editCommento.html";
		}
		else
			return "erroreCommento.html";
	}

	@RequestMapping(value="/editCommento/{id}", method = RequestMethod.POST)
	public String confermaModifica(@ModelAttribute("commento") Commento commento, 
			@PathVariable("id") Long id, Model model, Principal principal) {
		
		commento.setUtente(commentoService.commentoById(id).getUtente());
		commento.setVino(commentoService.commentoById(id).getVino());
			
		Long idVino = commentoService.commentoById(id).getVino().getId();
		//commentoService.update(commento, id);
		
		commentoService.inserisci(commento);
		logger.debug("ecco il commento modificato:" +commento.getTesto());
		return "redirect:/vino/"+idVino;
	}


	/**CANCELLAZIONE**/
	@RequestMapping(value="/cancellaCommento/{id}", method = RequestMethod.GET)
	public String cancellaCommento(@PathVariable("id")Long id, Model model, Principal principal) {
		Long idVino = commentoService.commentoById(id).getVino().getId();
		Credentials utenteCorrente = this.getCredentialsCorrente();
		Commento commento = this.commentoService.commentoById(id);

		if(this.commentoService.tuttiPerCredentials(utenteCorrente).contains(commento) || this.getCredentialsCorrente().getRole().equals("ADMIN")) {
			commentoService.cancella(commentoService.commentoById(id));
			return "redirect:/vino/"+idVino;
		}
		else
			return "erroreCommento.html";
	}
}
