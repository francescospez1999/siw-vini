package it.uniroma3.siw.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.spring.model.Credentials;
import it.uniroma3.siw.spring.service.CommentoService;
import it.uniroma3.siw.spring.service.CredentialsService;
import it.uniroma3.siw.spring.service.VinoService;

@Controller
public class SiwmelierController {

	@Autowired
	private VinoService vinoService;
	
	@Autowired
	private CredentialsService credentialsService;
	
	@Autowired
	private CommentoService commentoService;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	

	@RequestMapping(value= {"/", "index"}, method = RequestMethod.GET)
	public String getViniIndex(Model model) {
		model.addAttribute("viniDecr", this.vinoService.tuttiOrdinatiPerVotoDec());
		model.addAttribute("viniCresc", this.vinoService.tuttiOrdinatiPerVotoCres());
		return "index";
	}
	
	@RequestMapping(value="/profilo", method = RequestMethod.GET)
	public String getProfilo(Model model) {
		User current = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Credentials credentialsCorrente = this.credentialsService.getCredentials(current.getUsername());
		it.uniroma3.siw.spring.model.User utenteCorrente = credentialsCorrente.getUser();
		logger.debug("Passo a profilo utente: "+utenteCorrente.toString());
		
		model.addAttribute("credentials", credentialsCorrente);
		model.addAttribute("user", utenteCorrente);
		model.addAttribute("commenti", this.commentoService.tuttiPerCredentials(credentialsCorrente));
		return "profilo.html";
	}
}
