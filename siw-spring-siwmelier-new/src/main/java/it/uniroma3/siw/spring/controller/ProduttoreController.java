package it.uniroma3.siw.spring.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import it.uniroma3.siw.spring.model.*;
import it.uniroma3.siw.spring.service.*;
import it.uniroma3.siw.spring.validator.ProduttoreValidator;

@Controller
public class ProduttoreController {

	@Autowired
	private ProduttoreService produttoreService;

	@Autowired
	private VinoService vinoService;

	@Autowired
	private RegioneService regioneService;

	@Autowired
	private ProduttoreValidator produttoreValidator;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/*Si occupa di gestire la richiesta quando viene selezionato
	 * un produttore dalla pagina dei vari produttori*/
	@RequestMapping(value = "/produttore/{id}", method = RequestMethod.GET)
	public String getProduttore(@PathVariable("id") Long id, Model model) {
		
		if(produttoreService.produttorePerId(id) != null) {
		Produttore produttore = this.produttoreService.produttorePerId(id);
		model.addAttribute("produttore", produttore);
		model.addAttribute("vini", this.vinoService.viniPerProduttore(produttore));
		return "produttore.html";
		}
		
		else {
			model.addAttribute("errore", "produttore");
			return "erroreBase.html";
		}
	}

	/*Si occupa di gestire la richiesta quando viene selezionato
	 * il link della pagina produttori*/
	@RequestMapping(value = "/produttori", method = RequestMethod.GET)
	public String getProduttori(Model model) {
		model.addAttribute("produttori", this.produttoreService.tutti());
		return "produttori.html";
	}

	/**AGGIUNTA**/
	/*Popola la form*/
	@RequestMapping(value="/admin/addProduttore", method = RequestMethod.GET)
	public String addProduttore(Model model) {
		logger.debug("PASSO ALLA FORM addProduttore");
		model.addAttribute("produttore", new Produttore());
		model.addAttribute("vini", vinoService.tuttiOrdinatiAlfabetico());
		model.addAttribute("regioni", this.regioneService.tutteAlfabetico());
		return "/admin/produttoreForm.html";
	}

	/*raccoglie e valida i dati della form*/
	@RequestMapping(value = "/admin/inserisciProduttore", method = RequestMethod.POST)
	public String newProduttore(@ModelAttribute("produttore") Produttore produttore, 
			Model model, BindingResult bindingResult) {
		this.produttoreValidator.validate(produttore, bindingResult);
		if (!bindingResult.hasErrors()) {
			logger.debug("Non ci sono errori, passo alla conferma");
			produttore.setNome(produttore.getNome().toLowerCase());
			this.produttoreService.inserisci(produttore);

			//devo settare la many to many, quindi prendo prima tutte
			//le regioni che ho selezionato dalla form
			List<Regione> regioni = produttore.getRegioni();

			//poi per ogni regioni prendo i produttori esistenti
			for(Regione r : regioni) {
				List<Produttore> produttori = this.produttoreService.produttoriPerRegione(r);

				//ci aggiungo quello corrente alla lista
				produttori.add(produttore);

				//faccio il set della lista aggiornata
				r.setProduttori(produttori);

				//il metodo save se già esiste un oggetto con quell'id
				//funziona come un UPDATE
				regioneService.inserisci(r);
			}

			//IDEA SIMILE A QUELLA DELLE REGIONI
			List<Vino> viniProd = produttore.getViniProdotti();
			for(Vino v : viniProd) {
				v.setProduttore(produttore);
				vinoService.inserisci(v);
			};
			return "redirect:/home";
		}
		return "/admin/produttoreForm.html";
	}

	/**CANCELLAZIONE**/
	@RequestMapping(value="/admin/cancellaProduttori", method = RequestMethod.GET)
	public String cancellaProduttori(Model model) {
		model.addAttribute("produttori", produttoreService.tutti());
		return "admin/cancellaProduttori.html";
	}

	@RequestMapping(value="/admin/cancellaProduttore/{id}", method = RequestMethod.GET)
	public String cancellaProduttore(@PathVariable("id") Long id, Model model) {

		for(Regione regione : regioneService.regioniPerProduttori(produttoreService.produttorePerId(id))) {
			List<Produttore> produttori = produttoreService.produttoriPerRegione(regione);
			produttori.remove(produttoreService.produttorePerId(id));

			regione.setProduttori(produttori);
			regioneService.inserisci(regione);
		}
		produttoreService.cancellaPerId(id);
		model.addAttribute("produttori", produttoreService.tutti());
		return "redirect:/home";
	}


	/**MODIFICA**/
	@RequestMapping(value="/admin/editProduttori", method = RequestMethod.GET)
	public String editProduttori(Model model) {
		model.addAttribute("produttori", this.produttoreService.tutti());
		return "admin/editProduttori.html";
	}

	@RequestMapping(value="/admin/editProduttore/{id}", method = RequestMethod.GET)
	public String getEditProduttore(Model model,@PathVariable("id")Long id) {
		model.addAttribute("produttore", this.produttoreService.produttorePerId(id));
		model.addAttribute("regioni", this.regioneService.tutteAlfabetico());
		return "/admin/editProduttore.html";
	}

	@RequestMapping(value="/admin/editProduttore/{id}", method = RequestMethod.POST)
	public String editProduttore(Model model, @PathVariable("id") Long id,
			@RequestParam("nuovoNome")String nuovoNome,
			@RequestParam("nuovaDesc")String nuovaDesc,
			@RequestParam(value="nuoveReg", required=false)List<Long> idRegioni) {
		Produttore produttore = this.produttoreService.produttorePerId(id);
		List<Regione> regioni = produttore.getRegioni();

		//dobbiamo aggiornare la many-to-many
		if(idRegioni != null) {
			for(Long idReg : idRegioni) {
				Regione r = this.regioneService.regionePerId(idReg);
				if(!(regioni.contains(r))) {
					regioni.add(r);

					List<Produttore> produttori = r.getProduttori();
					produttori.add(produttore);
					r.setProduttori(produttori);
				}
			}
		}
		produttore.setNome(nuovoNome);
		produttore.setDescrizione(nuovaDesc);
		produttore.setRegioni(regioni);
		this.produttoreService.inserisci(produttore);

		return "redirect:/produttore/"+id;
	}
}
