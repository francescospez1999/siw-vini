package it.uniroma3.siw.spring.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "cataloghi")
public class Catalogo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@Column(length = 4096)
	private String descrizione;
	
	private String immagine;
	
	@OneToMany(mappedBy = "catalogo", cascade = CascadeType.REFRESH)
	private List<Vino> vini;
	
	public Catalogo() {
		
	}
	
	public Catalogo(String nome, String descrizione) {
		this.nome = nome;
		this.descrizione = descrizione;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<Vino> getVini() {
		return vini;
	}

	public void setVini(List<Vino> vini) {
		this.vini = vini;
	}

	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}
	
}
