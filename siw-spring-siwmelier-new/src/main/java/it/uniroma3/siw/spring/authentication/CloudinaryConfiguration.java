package it.uniroma3.siw.spring.authentication;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.cloudinary.*;
import com.cloudinary.utils.ObjectUtils;

@Configuration
public class CloudinaryConfiguration {
	
	@Bean
	public Cloudinary cloudinary() {
		return new Cloudinary(ObjectUtils.asMap(
			  "cloud_name", "sommeliar",
			  "api_key", "626669134736237",
			  "api_secret", "u9GxE_ZuuE1CjWDcpfcnSGelbzE"));
	}
}
