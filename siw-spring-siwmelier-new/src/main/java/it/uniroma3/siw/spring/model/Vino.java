package it.uniroma3.siw.spring.model;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "vini")

public class Vino {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@Column(length = 4096)
	private String descrizione;
	
	private Integer annoImbottigliamento;
	
	//@Column(nullable=false)
	private Float voto;
	
	private Float gradazione;
	
	//@Column(nullable=false)
	private String colore;
	
	//@Column(nullable=false)
	private Float effervescenza;
	
	//@Column(nullable=false)
	private Float corposita;
	
	private String immagine;
	
	@ManyToOne
	private Catalogo catalogo;
	
	@OneToMany(mappedBy = "vino", cascade=CascadeType.REMOVE)
	private List<Commento> commenti;
	
	@ManyToMany
	private List<Piatto> piatti;
	
	@ManyToOne
	private Produttore produttore;

	public Vino(String nome,String immagine,Float voto) {
		this.nome = nome;
		this.immagine = immagine;
		this.voto=voto;
	}
	
	public Vino() {
		this.piatti = new ArrayList<>();
		this.commenti = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Integer getAnnoImbottigliamento() {
		return annoImbottigliamento;
	}

	public void setAnnoImbottigliamento(Integer annoImbottigliamento) {
		this.annoImbottigliamento = annoImbottigliamento;
	}

	public Float getVoto() {
		return voto;
	}

	public void setVoto(Float voto) {
		this.voto = voto;
	}

	public Float getGradazione() {
		return gradazione;
	}

	public void setGradazione(Float gradazione) {
		this.gradazione = gradazione;
	}

	public String getColore() {
		return colore;
	}

	public void setColore(String colore) {
		this.colore = colore;
	}

	public Float getEffervescenza() {
		return effervescenza;
	}

	public void setEffervescenza(Float effervescenza) {
		this.effervescenza = effervescenza;
	}

	public Float getCorposita() {
		return corposita;
	}

	public void setCorposita(Float corposita) {
		this.corposita = corposita;
	}

	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}

	public Produttore getProduttore() {
		return produttore;
	}

	public void setProduttore(Produttore produttore) {
		this.produttore = produttore;
	}

	public List<Piatto> getPiatti() {
		return piatti;
	}

	public void setPiatti(List<Piatto> piatti) {
		this.piatti = piatti;
	}

	public Catalogo getCatalogo() {
		return catalogo;
	}

	public void setCatalogo(Catalogo catalogo) {
		this.catalogo = catalogo;
	}

	public List<Commento> getCommenti() {
		return commenti;
	}

	public void setCommenti(List<Commento> commenti) {
		this.commenti = commenti;
	}
	
	@Override
	public String toString() {
		String cap = nome.substring(0, 1).toUpperCase() + nome.substring(1);
		
		return " " + cap +" ";
	}

}
