package it.uniroma3.siw.spring.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.spring.model.Commento;
import it.uniroma3.siw.spring.model.Credentials;
import it.uniroma3.siw.spring.model.Vino;
import it.uniroma3.siw.spring.repository.CommentoRepository;

@Service
public class CommentoService {

	@Autowired
	private CommentoRepository commentoRepository;

	@Transactional(isolation = Isolation.REPEATABLE_READ)
	public Commento inserisci(Commento commento) {
		return this.commentoRepository.save(commento);
	}

	@Transactional
	public List<Commento> tutti(){
		return this.commentoRepository.findAll();
	}

	@Transactional
	public List<Commento> tuttiPerVino(Vino vino){
		return this.commentoRepository.findByVino(vino);
	}

	@Transactional
	public List<Commento> tuttiPerCredentials(Credentials credentials){
		return this.commentoRepository.findByUtente(credentials);
	}


	@Transactional
	public Commento commentoById(Long id) {
		Optional<Commento> optional = this.commentoRepository.findById(id);
		if(optional.isPresent()) {
			return optional.get();
		}

		else {
			return null;
		}
	}

	@Transactional
	public void cancella(Commento commento) {
		this.commentoRepository.delete(commento);
	}
	
	@Transactional
	public List<Commento> tuttiPerVinoUtenteCrescente(Vino vino){
		return this.commentoRepository.findByVinoOrderByUtenteAsc(vino);
	}
}
