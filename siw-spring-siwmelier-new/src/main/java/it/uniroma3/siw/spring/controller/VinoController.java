package it.uniroma3.siw.spring.controller;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.siw.spring.model.Catalogo;
import it.uniroma3.siw.spring.model.Credentials;
import it.uniroma3.siw.spring.model.Piatto;
import it.uniroma3.siw.spring.model.Produttore;
import it.uniroma3.siw.spring.model.Vino;
import it.uniroma3.siw.spring.service.CatalogoService;
import it.uniroma3.siw.spring.service.CloudinaryService;
import it.uniroma3.siw.spring.service.CommentoService;
import it.uniroma3.siw.spring.service.CredentialsService;
import it.uniroma3.siw.spring.service.PiattoService;
import it.uniroma3.siw.spring.service.ProduttoreService;
import it.uniroma3.siw.spring.service.VinoService;
import it.uniroma3.siw.spring.validator.VinoValidator;

@Controller
public class VinoController {

	@Autowired
	private VinoService vinoService;

	@Autowired
	private VinoValidator vinoValidator;

	@Autowired
	private ProduttoreService produttoreService;

	@Autowired
	private CatalogoService catalogoService;

	@Autowired
	private CloudinaryService cloudinaryService;

	@Autowired
	private CredentialsService credentialsService;
	
	@Autowired
	private PiattoService piattoService;

	@Autowired
	private CommentoService commentoService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private Vino vinoTemp;
	
	private int anno = LocalDate.now().getYear();

	private Credentials getCredentialsCorrente() {
		User current = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return this.credentialsService.getCredentials(current.getUsername());
	}
	
	/*Si occupa di gestire la richiesta quando viene selezionato
	 * un vino dalla pagina dei vari vini*/
	@RequestMapping(value = "/vino/{id}", method = RequestMethod.GET)
	public String getVino(@PathVariable("id") Long id, Model model) {
		
		if(vinoService.vinoPerId(id) != null) {
		Vino vino = this.vinoService.vinoPerId(id);
		List<Piatto> piatti = vino.getPiatti();
		Produttore prod = vino.getProduttore();
		Credentials credCorrente = this.getCredentialsCorrente();
		
		model.addAttribute("vino", vino);
		model.addAttribute("piatti", piatti);
		model.addAttribute("produttore", prod);
		model.addAttribute("commenti",this.commentoService.tuttiPerVinoUtenteCrescente(vino));
		model.addAttribute("credentialsCorrente", credCorrente);
		return "vino.html";
		}
		
		else {
			model.addAttribute("errore", "vino");
			return "erroreBase.html";
		}
	}

	/*Si occupa di gestire la richiesta quando viene selezionato
	 * il link della pagina vini*/
	@RequestMapping(value = "/vini", method = RequestMethod.GET)
	public String getVini(Model model) {
		model.addAttribute("vini", this.vinoService.tutti());
		return "vini.html";
	}

	/*gestisce la richiesta da parte della vista per la ricerca
	 * con una parola chiave*/
	@RequestMapping(value="/ricerca", method = RequestMethod.GET)
	public String getRicerca(Model model,@RequestParam(value="cerca") String cerca) {
		model.addAttribute("vini", this.vinoService.trovaPerNomeLike(cerca.toLowerCase()));
		logger.debug("hai cercato: "+cerca);
		model.addAttribute("cerca", cerca.toLowerCase());
		return "ricerca.html";
	}

	@RequestMapping(value = "/viniAlfabetico", method = RequestMethod.GET)
	public String getViniAlfa(Model model) {
		model.addAttribute("vini", this.vinoService.tuttiOrdinatiAlfabetico());
		return "vini.html";
	}
	
	@RequestMapping(value = "/viniVotoCrescente", method = RequestMethod.GET)
	public String getViniNomeCresc(Model model) {
		model.addAttribute("vini", this.vinoService.tuttiOrdinatiPerVotoCres());
		return "vini.html";
	}

	@RequestMapping(value = "/viniVotoDecrescente", method = RequestMethod.GET)
	public String getViniNomeDecresc(Model model) {
		model.addAttribute("vini", this.vinoService.tuttiOrdinatiPerVotoDec());
		return "vini.html";
	}


	/**AGGIUNTA**/
	/*Popola la form*/
	@RequestMapping(value="/admin/addVino", method = RequestMethod.GET)
	public String addVino(Model model) {
		logger.debug("PASSO ALLA FORM addVino");
		model.addAttribute("vino", new Vino());
		model.addAttribute("piatti", this.piattoService.tutti());
		model.addAttribute("produttori", this.produttoreService.tutti());
		model.addAttribute("cataloghi", this.catalogoService.tutti());
		model.addAttribute("massimoAnno", this.anno);
		return "/admin/vinoForm.html";
	}

	/*raccoglie e valida i dati della form*/
	@RequestMapping(value = "/admin/inserisciVino", method = RequestMethod.POST)
	public String newProduttore(@ModelAttribute("vino") Vino vino, 
			Model model, BindingResult bindingResult,
			@RequestParam(value="foto")MultipartFile foto) {
		this.vinoValidator.validate(vino, bindingResult);

		if (!bindingResult.hasErrors()) {
			logger.debug("Non ci sono errori, passo alla conferma");
			this.vinoTemp = vino;
			this.vinoTemp.setImmagine(this.cloudinaryService.salvaImmagine(foto));

			model.addAttribute("vino",vino);
			model.addAttribute("produttore", vino.getProduttore());
			model.addAttribute("piatti", vino.getPiatti());
			return "admin/confermaVinoForm.html";
		}
		model.addAttribute("piatti", this.piattoService.tutti());
		model.addAttribute("produttori", this.produttoreService.tutti());
		model.addAttribute("cataloghi", this.catalogoService.tutti());
		model.addAttribute("massimoAnno", this.anno);
		return "admin/vinoForm.html";
	}

	@RequestMapping(value = "/admin/confermaVino", method = RequestMethod.POST)
	public String confermaVino(Model model,
			@RequestParam(value = "action") String comando) {
		model.addAttribute("vino",vinoTemp);

		if(comando.equals("confirm")) {
			logger.debug("CONFERMO e SALVO dati vino");
			this.vinoService.inserisci(vinoTemp);

			//aggiungo il vino ai vini del produttore
			Produttore p = this.vinoTemp.getProduttore();
			List<Vino> vini = this.vinoService.viniPerProduttore(p);
			vini.add(vinoTemp);
			p.setViniProdotti(vini);
			this.produttoreService.inserisci(p);

			//aggiungo i piatti
			List<Piatto> piatti = this.vinoTemp.getPiatti();
			for(Piatto piatto : piatti) {
				List<Vino> viniPiatti = this.vinoService.viniPerPiatti(piatto);
				viniPiatti.add(vinoTemp);
				piatto.setVini(viniPiatti);
				this.piattoService.inserisci(piatto);
			}

			//aggiorno il catalogo
			Catalogo c = this.vinoTemp.getCatalogo();
			List<Vino> viniCat = this.vinoService.viniPerCatalogo(c);
			viniCat.add(vinoTemp);
			c.setVini(viniCat);
			catalogoService.inserisci(c);

			return "redirect:/vini";
		}
		else {
			model.addAttribute("piatti", this.piattoService.tutti());
			model.addAttribute("produttori", this.produttoreService.tutti());
			model.addAttribute("cataloghi", this.catalogoService.tutti());
			return "/admin/vinoForm.html";
		}
	}


	/**CANCELLAZIONE**/
	@RequestMapping(value="/admin/cancellaVini", method = RequestMethod.GET )
	public String cancellaVini(Model model) {
		model.addAttribute("vini", this.vinoService.tutti());
		return "/admin/cancellaVini.html";
	}

	@RequestMapping(value="/admin/cancellaVino/{id}", method =  RequestMethod.GET )
	public String cancellaVino(Model model, @PathVariable("id")Long id) {
		logger.debug("***cancello vino con id:***"+id);
		this.vinoService.cancellaPerId(id);
		return "redirect:/admin/cancellaVini";
	}


	/**MODIFICA**/
	@RequestMapping(value="/admin/editVini", method =  RequestMethod.GET)
	public String editVini(Model model) {	
		model.addAttribute("vini", this.vinoService.tutti());
		return "/admin/editVini.html";
	}

	@RequestMapping(value="/admin/editVino/{id}", method = RequestMethod.GET)
	public String getEditVino(Model model,@PathVariable("id")Long id) {
		model.addAttribute("vino", this.vinoService.vinoPerId(id));
		model.addAttribute("cataloghi", this.catalogoService.tuttiCresc());
		model.addAttribute("maxAnno", this.anno);
		return "/admin/editVino.html";
	}

	@RequestMapping(value="/admin/editVino/{id}", method = RequestMethod.POST)
	public String editVino(@PathVariable("id")Long id,
			@Validated @ModelAttribute("vino") Vino vino,Model model) {
			logger.debug(" id vino: " + vino.getId() + " id produttore: " + vino.getProduttore() + " piatti: " + vino.getPiatti());
			
			Vino vinoModificato = vinoService.vinoPerId(id);
			vino.setProduttore(vinoModificato.getProduttore());
			vino.setImmagine(vinoModificato.getImmagine());
			vino.setPiatti(vinoModificato.getPiatti());
			
			logger.debug(" id vino: " + vino.getId() + " id produttore: " + vino.getProduttore() + " url immagine: " + vino.getImmagine());
			
			vinoService.inserisci(vino);
			return "redirect:/admin/editVini";
	}
}
