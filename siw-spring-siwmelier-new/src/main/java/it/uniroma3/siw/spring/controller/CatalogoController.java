package it.uniroma3.siw.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import it.uniroma3.siw.spring.model.Catalogo;
import it.uniroma3.siw.spring.service.CatalogoService;
import it.uniroma3.siw.spring.service.VinoService;
import it.uniroma3.siw.spring.validator.CatalogoValidator;

@Controller
public class CatalogoController {

	@Autowired
	private CatalogoService catalogoService;

	@Autowired
	private VinoService vinoService;

	@Autowired
	private CatalogoValidator catalogoValidator;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@SuppressWarnings("unused")
	private void aggiungiCataloghi() {
		Catalogo c1 = new Catalogo("doc", "denominazione origine controllata");
		c1.setImmagine("https://i.postimg.cc/SsCJBH91/doc.png");
		Catalogo c2 = new Catalogo("docg", "denominazione origine controllata e garantita");
		c2.setImmagine("https://i.postimg.cc/wMz7gFcq/docg.png");
		Catalogo c3 = new Catalogo("igp","indicazione geografica protetta");
		c3.setImmagine("https://i.postimg.cc/SRGjs8DW/igp.png");
		Catalogo c4 = new Catalogo("vdt", "vino da tavola");
		c4.setImmagine("https://i.postimg.cc/7P15m8wf/vdt.png");
		this.catalogoService.inserisci(c1);
		this.catalogoService.inserisci(c2);
		this.catalogoService.inserisci(c3);
		this.catalogoService.inserisci(c4);
	}

	/*Si occupa di gestire la richiesta quando viene selezionato
	 * un catalogo dalla pagina dei vari cataloghi*/
	@RequestMapping(value = "/catalogo/{id}", method = RequestMethod.GET)
	public String getCatalogo(@PathVariable("id") Long id, Model model) {
		
		if(catalogoService.catalogoPerId(id) != null) {
		Catalogo catalogo = this.catalogoService.catalogoPerId(id);
		model.addAttribute("catalogo", catalogo);
		/*popola la lista dei vini di questo catalogo corrente*/
		model.addAttribute("vini",this.vinoService.viniPerCatalogo(catalogo));
		return "catalogo.html";
		}
		
		else {
			model.addAttribute("errore", "catalogo");
			return "erroreBase.html";
		}
	}

	/*Si occupa di gestire la richiesta quando viene selezionato
	 * il link della pagina cataloghi*/
	@RequestMapping(value = "/cataloghi", method = RequestMethod.GET)
	public String getCataloghi(Model model) {
		//this.aggiungiCataloghi();
		model.addAttribute("cataloghi", this.catalogoService.tuttiCresc());
		return "cataloghi.html";
	}


	/**AGGIUNTA**/
	/*Popola la form*/
	@RequestMapping(value="/admin/addCatalogo", method = RequestMethod.GET)
	public String addCatalogo(Model model) {
		logger.debug("PASSO ALLA FORM addCatalogo");
		model.addAttribute("catalogo", new Catalogo());
		return "/admin/catalogoForm.html";
	}

	/*raccoglie e valida i dati della form*/
	@RequestMapping(value = "/admin/inserisciCatalogo", method = RequestMethod.POST)
	public String newCatalogo(@ModelAttribute("catalogo") Catalogo catalogo, 
			Model model, BindingResult bindingResult) {
		this.catalogoValidator.validate(catalogo, bindingResult);
		if (!bindingResult.hasErrors()) {
			logger.debug("Non ci sono errori, inserisco il catalogo nel db");
			this.catalogoService.inserisci(catalogo);
			return "redirect:/cataloghi";
		}
		return "/admin/catalogoForm.html";
	} 
}
