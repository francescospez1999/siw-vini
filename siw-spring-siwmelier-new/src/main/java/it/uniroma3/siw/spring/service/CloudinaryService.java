package it.uniroma3.siw.spring.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

@Service 
public class CloudinaryService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass()); 
	private final Cloudinary cloudinary;

	public CloudinaryService(final Cloudinary cloudinary) { 
		this.cloudinary = cloudinary; 
	}

	public String salvaImmagine(final MultipartFile file) {
		logger.debug("****Inizio salvataggio immagine****"); 

		try 
		{
			return (String) this.cloudinary.uploader().upload(convert(file), ObjectUtils.emptyMap()).get("secure_url"); 
		}	 

		catch (IOException e) {

			throw new RuntimeException("****Errore caricamento immagine****");
		}
	}

	private File convert(final MultipartFile file) throws IOException {
		logger.debug("****Inizio conversione****"); 
		File convFile = new File(file.getOriginalFilename()); 

		try (FileOutputStream fos = new FileOutputStream(convFile)) {
			fos.write(file.getBytes());
		}
		logger.debug("****Conversione terminata****"); 
		return convFile;
	}
}