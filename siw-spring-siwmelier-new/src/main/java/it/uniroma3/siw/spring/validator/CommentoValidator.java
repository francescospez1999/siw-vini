package it.uniroma3.siw.spring.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.spring.model.Commento;

@Component
public class CommentoValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Commento.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
	}

}
