package it.uniroma3.siw.spring.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "regioni")
public class Regione {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	private String immagine;
	
	@ManyToMany(cascade = CascadeType.REFRESH)
	private List<Produttore> produttori;

	public Regione() {
		
	}
	
	public Regione(String nome) {
		this.nome=nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Produttore> getProduttori() {
		return produttori;
	}

	public void setProduttori(List<Produttore> produttori) {
		this.produttori = produttori;
	}

	@Override
	public String toString() {
		String cap = nome.substring(0, 1).toUpperCase() + nome.substring(1);
		
		return " " + cap +" ";
	}

	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((immagine == null) ? 0 : immagine.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((produttori == null) ? 0 : produttori.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Regione other = (Regione) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (immagine == null) {
			if (other.immagine != null)
				return false;
		} else if (!immagine.equals(other.immagine))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (produttori == null) {
			if (other.produttori != null)
				return false;
		} else if (!produttori.equals(other.produttori))
			return false;
		return true;
	}
	
}
