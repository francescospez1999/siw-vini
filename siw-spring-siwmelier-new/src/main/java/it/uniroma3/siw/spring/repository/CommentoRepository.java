package it.uniroma3.siw.spring.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.spring.model.Commento;
import it.uniroma3.siw.spring.model.Credentials;
import it.uniroma3.siw.spring.model.Vino;

public interface CommentoRepository extends CrudRepository<Commento, Long>{
	
	public List<Commento> findByVino(Vino vino);
	
	public List<Commento> findByUtente(Credentials credentials);
	
	public List<Commento> findAll();
	
	public List<Commento> findByVinoOrderByUtenteAsc(Vino vino);

}
