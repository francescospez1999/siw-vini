package it.uniroma3.siw.spring.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import it.uniroma3.siw.spring.model.*;
import it.uniroma3.siw.spring.service.*;
import it.uniroma3.siw.spring.validator.RegioneValidator;

@Controller
public class RegioneController {

	@Autowired
	private RegioneService regioneService;

	@Autowired
	private ProduttoreService produttoreService;

	@Autowired
	private RegioneValidator regioneValidator;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/*funzione test per aggiungere regioni*/
	@SuppressWarnings("unused")
	private void aggiungiRegioniTest() {
		List<String> regioni = Arrays.asList("valle d'aosta","piemonte",
				"lombardia","trentino","veneto","friuli","liguria",
				"emilia","toscana","marche","umbria","lazio","abruzzo",
				"molise","campania","puglia","basilicata","calabria","sicilia",
				"sardegna");
		for(String r : regioni) {
			Regione reg = new Regione(r);
			regioneService.inserisci(reg);
		}
	}

	/*Si occupa di gestire la richiesta quando viene selezionato
	 * una regione dalla pagina delle varie regioni*/
	@RequestMapping(value = "/regione/{id}", method = RequestMethod.GET)
	public String getRegione(@PathVariable("id") Long id, Model model) {
		if(regioneService.regionePerId(id) != null) {
		Regione regione = this.regioneService.regionePerId(id);
		model.addAttribute("regione", regione);
		List<Produttore> produttori = this.produttoreService.produttoriPerRegione(regione);
		logger.debug(regione.getNome()+" con i seguenti produttori:" + produttori.toString());
		
		model.addAttribute("produttori", produttori);
		return "regione.html";
		}
		
		else {
			model.addAttribute("errore", "luogo");
			return "erroreBase.html";
		}
	}

	/*Si occupa di gestire la richiesta quando viene selezionato
	 * il link della pagina regioni*/
	@RequestMapping(value = "/regioni", method = RequestMethod.GET)
	public String getRegioni(Model model) {
		model.addAttribute("regioni", this.regioneService.tutteAlfabetico());
//		aggiungiRegioniTest();
		return "regioni.html";
	}

	
	/**AGGIUNTA**/
	/*Popola la form*/
	@RequestMapping(value="/admin/addRegione", method = RequestMethod.GET)
	public String addRegione(Model model) {
		logger.debug("PASSO ALLA FORM addRegione");
		model.addAttribute("regione", new Regione());
		return "/admin/regioneForm.html";
	}

	/*raccoglie e valida i dati della form*/
	@RequestMapping(value = "/admin/inserisciRegione", method = RequestMethod.POST)
	public String newRegione(@ModelAttribute("regione") Regione regione, 
			Model model, BindingResult bindingResult) {
		this.regioneValidator.validate(regione, bindingResult);
		if (!bindingResult.hasErrors()) {
			logger.debug("Non ci sono errori, inserisco la regione nel db");
			regione.setNome(regione.getNome().toLowerCase());
			return "redirect:/regioni";
		}
		return "/admin/regioneForm.html";
	} 

}
